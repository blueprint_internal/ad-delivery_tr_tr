
function WeightScale(resources)
{
	WeightScale.resources = resources;
}
WeightScale.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 400, Phaser.CANVAS, 'WeightScale', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},

	preload: function()
	{
		this.game.scale.maxWidth = 800;
    	this.game.scale.maxHeight = 400;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    	this.game.load.image('base', WeightScale.resources.base);
    	this.game.load.image('arm', WeightScale.resources.arm);
    	this.game.load.image('plate', WeightScale.resources.plate);
    	this.game.stage.backgroundColor = '#ffffff'
	},

	create: function(evt)
	{
		this.game.stage.backgroundColor = '#ffffff' 
		this.balanceG = this.game.add.group();
		this.arm = this.game.add.sprite(0,0, 'arm');
		this.plate_1 = this.game.add.sprite(0,0, 'plate');
		this.plate_2 = this.game.add.sprite(0, 0, 'plate');
		this.base = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'base');
		this.balanceG.add(this.arm);
		
		this.balanceG.add(this.plate_1);
		this.balanceG.add(this.plate_2);
		this.base.anchor.set(0.5);
		this.arm.x= 0;
		this.arm.y = 0;
		this.arm.rotation = 0;
		this.parent.arm = this.arm;
		this.parent.plate_1 = this.plate_1;
		this.parent.plate_2 = this.plate_2;
		this.parent.balanceG = this.balanceG;
		this.parent.buildAnimation();

    	
		
	},

	buildAnimation: function()
	{

		this.plate_1.x = (this.plate_1.width)+6;
		this.plate_1.y = 1;
		this.plate_1.anchor.set(0.5);
		this.plate_1.anchor.y-=.53;
		this.plate_1.rotation +=.3//

		this.plate_2.x = -((this.plate_2.width+8));
		this.plate_2.y = 1;
		this.plate_2.anchor.set(0.5);
		this.plate_2.anchor.y-=.53;
		this.plate_2.rotation +=.3;

		var style = { font: "bold 19px freight-sans-pro", fill: "#000000", wordWrap: true, wordWrapWidth: this.plate_1.width, align: "center",lineSpacing: -10 };

		var field_1 = this.game.make.text(0, 0, WeightScale.resources.rightText, style);
		field_1.anchor.set(0.5);
		field_1.y = this.plate_1.height-50;
		this.plate_1.addChild(field_1);
		//
		var field_2 = this.game.make.text(0, 0, WeightScale.resources.leftText, style);
		field_2.anchor.set(0.5);
		field_2.y = this.plate_2.height-50;
		this.plate_2.addChild(field_2);

		this.arm.x-=this.arm.width/2;

		this.balanceG.x = this.game.world.centerX;
		this.balanceG.y = 60;

		this.balanceG.x = this.game.world.centerX;
		this.balanceG.rotation = -.3;


		var tween = this.game.add.tween(this.balanceG).to( { rotation: .3 }, 5000,Phaser.Easing.Back.InOut, true, 0, -1);
		var plate_1_animation = this.game.add.tween(this.plate_1).to( { rotation: -.3 }, 5000,Phaser.Easing.Back.InOut, true, 0, -1);
		var plate_2_animation = this.game.add.tween(this.plate_2).to( { rotation: -.3 }, 5000,Phaser.Easing.Back.InOut, true, 0, -1);
       
		tween.yoyo(true);
		plate_1_animation.yoyo(true);
		plate_2_animation.yoyo(true);

		this.animate();
	},

	animate: function()
	{
		//this.scaleAnimation.start();

	
	},

	update: function()
	{

	},

	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}


